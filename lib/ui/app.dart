import 'package:flutter/material.dart';
import 'package:image_viewer/model/image_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class App extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    String jsonRaw = '{ "id": 1, "url": "https://via.placeholder.com/600/92c952&quot;"}';
    var jsonObject = json.decode(jsonRaw);
    print(jsonObject);
    var image1 = ImageModel(jsonObject['id'], jsonObject['url']);
    var image2 = ImageModel.fromJson(jsonObject);
    List<ImageModel> images = [];
    images.add(image1);
    images.add(image2);

    var appWidget = MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('My Image Viewer - v0.0.8'),),
        body: ListView.builder(
          itemCount: 2,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              padding: const EdgeInsets.all(8.0),
              child: Image.network(images[index].url),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: null
        ),
      )
  );

    return appWidget;
  }
}