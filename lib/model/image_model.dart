class ImageModel {
  late int id;
  late String url;

  ImageModel(this.id, this.url);

  ImageModel.fromJson(jsonObject) {
    id = jsonObject['id'];
    url = jsonObject['url'];
  }
  //
  // String? getUrl() {
  //   return url;
  // }

  String toString() {
    return '($id, $url)';
  }
}